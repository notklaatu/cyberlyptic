Cyberlyptic
===============

In case you don't know, in the 1990s there was a big scare over what
would happen when all the digital clocks in the world ticked over from
1999 (``99``) to 2000 (``00``). People figured that computers would get
confused, think it was 1900, and fall apart at the seams, taking all
of the civilised world down with them. The "Big Media" branding for this
specially-crafted mass scare was "Y2K".

This part is real, by the way; this actually happened. This is not the
made-up part yet.

IT people scrambled to patch computers in the frantic hope that they
would continue to function after the change-over. Software companies
sold placebo software to calm the anxious public. Over the New Years
holidays, people *literally* left cosmopolitan areas for fear of
getting caught up in the mass chaos that they had been told would
happen after the clock ticked past midnight.

In what was truly the least climactic moment of the century, nothing
happened. Everything was fine.

Except in the world of **Cyberlyptic**.

**Cyberlyptic** is a fork of Earth's timeline (the one you live in
now), in which Y2K ripped the civilised world asunder, giving rise to
both the best and worst of humanity's technological *élan vital*.

So get out of your timeline and visit with me the gritty, miserable
alternate future that is the **Cyberlypse**.

Contents:

.. toctree::
   :maxdepth: 2

   index
   primer
   world
   money
   people
   places
   things
   canon
   licensing
   colophon
   
	      
Indices
=========

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
