The World After the End of the World
========================================

When Y2K ("Y2K" doesn't refer to the year, but the mass computer
crashes that happened at the start of the year 2000) happened, it
severely crippled consumer and federal technological systems, sending
the *status quo* into a tailspin. Retail came grinding to a halt,
factory production fell apart, communication systems crumbled, planes
literally fell out of the sky, militaries sank and governments
floundered.

The backbone of the more advanced segments of the tech industry and
tech elite was, conversely, almost completely unaffected, because
their systems ran operating systems independent of the restrictive
4-digit year format (they based their clocks on seconds rather than a
hard-coded, outdated Gregorian year designation).

In other words, when all the bad tech died and took down the flimsy social
constructs that had been build on top of it, the hard tech got
hardened. And the social constructs had to re-construct.

We call this the **Cyberlypse**.


Hardware
-------------

If you look back at the archives, you'll notice that the aesthetic of
electronics halted at 1999; consumer electronics in the **Cyberlytic**
world tend to be just small enough to be convenient, and yet somehow
too big to integrate the world around it. The hardware is often plain
beige, if it's from the civilian world, and black or camo-green if it
was inherited from the old militaries. You get some futuro-freaks who
fancy themselves special cases, and they sport white-and-chrome
hardware, but nobody takes them seriously for their tech (taking them
seriously for their :ref:`rek <money>`_, on the other hand, is a
different story).

Tech is commonplace and innovation is steady, but the emphasis on
making smaller and sleeker tech has been left by the wayside, largely
due to the fall of the big manufacturing factories that were pushing
smaller form factors. Microchip technology has basically stalled; 1
gHz or so is as good as it's gonna get for a while, so settle in.

The recycling and re-use of tech is an art form that the modern
resident of the **Cyberlypse** learns at least to get familiar
with. The days of mass-producing 83 different laptop models every
single calendar year are over. We get what we get, and if we don't get
anything then we dig something out of the rubbish.

Software
--------------

The big infrastructure computers are called *mainframes*; that's both
their form factor and suggestive of the OS they run. The OS is mostly
text-based, because the more we make our computers do, the less pretty
they get; think of an old coal miner. What I'm trying to say is, we
overwork our gear.

All code that you encounter in every day life is open source, because
nobody in real life can be bothered to invent a legal system for
"intellectual property". Some particularly skilled hackers sell their
code, but they're really just selling convenience, because
realistically, if you looked hard enough you could just grab it from
the :ref:`mesh <mesh>`_.

The computers and tech the average inhabitant uses shares a lineage
with the big data centers of old, and the modern bit pushers, so the
language of computech is mostly UNIX-like. Since some people emerged
from the tech fallout knew DOS better than they knew UNIX, DOS-like
commands also fit into the mainframe space; just flip those slashes
and you'll be fine (and if you forget to flip them, the mainframe has
a compatibility layer that does it for you).

.. note::

   Since backslashes are the same as forward slashes to maintain
   compatibility, there are no escape characters on a **Cyberlyptic**
   mainframe. A character that needs to be taken literally gets
   wrapped in double-quotes (`` "/" ``) (because that's literally what
   double-quotes are for).


.. _mesh:
   
Information Superhighway: The Mesh
-----------------------------------------

A complex network of networks (actually, meshes of meshes) that
connects universities, businesses, and people together, the
**Information Superhighway** (or just "the mesh" in casual
conversation, or "the highway" if you're an old-timer) is the online
home to all modern technologists, businesspeople, scientists, and, of
course, hackers and mesh-heads.

The mesh doesn't exist just on the mainframes. Each university,
EnterCorp, small business, police franchise, and so on, has their own
mainframe that hosts their unique individual node. Their power goes
off, their node goes offline. If their node is compromised, they've got
problems that could well affect their physical location, depending on
how their system is wired (and yeah, you guessed it: just because some
sub-contractor came in and set up your 'frame doesn't mean he knew his
way around a firewall).

Individual connections into the mesh are called "terminals"
(or just a "term", if you're hip. Hey, it was that or "inal".).

Terminals can take many forms; they might be a box that sits on your
desk at your 9-to-11 job, or it might be a custom rig that you wear on
your head with input sensors sewn into your gloves. These aren't
Nintendo VirtualBoys, though; while they may be as large (or not; the
professional boutique designed 'terms sit over one eye, like an
army-designed monocle from the future), they allow the user to see
both the physical and online world. Whether or not a user has the
alacrity to actually interact with both at the same time, however, is
anther question entirely.


Cybergenetics
-------------------

There's plenty of augmentation for the casual denizen (like wearable
terms, keyboard triggers in gloves, neon strips in coats, and other
fashion statements *a la* higher-function). When you see someone
tricked out with tech gear, you might say somethnig like "hey, look at
that guy over there; a real cyborg, huh?" but that's just an
expression. The clothes do not make the 'borg. Everyone knows the
real money's in cybergenetics.

Cybernetics [*sic*] experimented around with the maybe too-obvious idea of
taking clunky tech and sticking it inside people's bodies. Sometimes it
worked, other times limbs just ended up falling off. Rumour has it
that the old tunnels in a few big cities still have the mutant
descendants of experimental cybernetic patients living in them to this
day. Probably not true; it's not like you inherit cybernetic implants
via genetics, right?

Funny thing is, that's exactly what the scientists decided to do: make
it genetic. For decades, they worked on integrating meshry into the
fabric of human genetics. If you have enough the :ref:`Federeks
<money>`_ for it, you can purchase cybergenetic parts: limbs, eyes,
organs. They need to be programmed with ``BioAssembly``, not exactly a
common programming language and pretty well-guarded by the EnterCorps.

Mistakes and grafts gone horribly wrong are by no means unheard of; I
once knew a guy who knew a guy whose roommate saved up for years to get
an upgraded set of electric hands so he could do his graffiti
faster. Trouble was, this guy was a lefty, and the programmer forgot
to flip the "left handed" bit in the BioCode. Dude's body short
meshed within seconds. No more roommate.


Governance
----------------

After the tech fall of 2000, people figured out that their governments
weren't doing a heck of a lot, and, looking back at it, hadn't been for
decades. Once the governments were literally offline, they fell so out
of touch and nobody missed them that they eventually just faded away.

But nature abhors a vacuum. The only places online and capable of
communicating with other states and nations were the :ref:`Circuit
<mesh>`_ Providers and Mainframe owners. And they're the ones that
ended up, by hook or by crook, taking over.

This means there's not much unity in terms of how different regions of
the world (even regions on the same continent) are governed.

* The largest cities are almost always the ones that were adopted by
  an major corporation (an **EnterCorp**). They tend toward a style of
  government that is, to put it politely, one genocide away from
  Nazism. Big city life usually revolves around their parent
  corporation, and, naturally, the "friendly" competition against others.
* Smaller cities (**CityCorp**, even though they aren't always
  incorporated, strictly speaking) are usually those that got adopted
  by a local uni, or a local mesh station, or whatever local
  establishment with a mainframe and money enough to keep
  infrastructure alive. Lots of variety in how these places are run,
  ranging from isolationism to sniper-like acquistions of neighbouring
  CityCorps.
* Every thing else is generally just lumped together as the
  **Outland**, or **Bush** if you're cheeky. Big city folk think it's
  tantamount to a no man's land where cannibals roam and a term is
  never to be seen, but in reality it's full of variety just like
  everything else. Some towns have a mainframe and are very active in
  cyberspace, others maintain their own local network and keep to
  themselves, some are ghostowns, others are the outposts to
  activities you and I will likely never even know about. There's a
  whole world out there, kids.


.. _money:

Money
-----------

The world has transitioned over to The Federek, an entirely digital
currency.

.. note::

   The monetary system was created for `Epoch:Human
   <http://epoch-human.wikia.com/wiki/The_Federek>`_, through a
   collaboration between `Thirdilemma
   <http://www.reddit.com/user/Thirdilemma>`_, `WhatWhatHunchHunch
   <http://www.reddit.com/user/WhatWhatHunchHunch>`_, and
   `ViolatorMachine <http://www.reddit.com/user/ViolatorMachine>`_.

The Federek System encompasses the officially agreed upon world currency
called the Federal and the underground monetary network called
SafeHaven. 

Officially, the Federek uses a stylized ``F`` as its currency symbol:

.. image:: federek.png

Lacking that symbol on keyboards, the *ḟ* (that's Unicode
``0x1E1F``, or Compose-dot-f on Linux) character is acceptable, or
just *f* in a pinch.

The official name of the currency is "Federek". In polite company, it
is referred to by its full name, but on the streets, the vulgar terms
for it are "rek", "rekko", "rekky", or even a "fed", depending on
local slang habits. These are usually considered swear words, as well,
since the Federek is regarded as a diety.

The exact monetary value of the Federek fluctuates on an almost hour-by-hour
basis. Manipulation of the worldwide stock markets by computerised
trading systems create fund fluctuations almost too fast to follow.

There's an underground network for monetary exchange, know as
SafeHaven. This as a hardware-based monetary system that logs goods
and services for exchange on a series of mobile mainframes, enabling
transfer from peer to peer outside the Federek system. Its official
purpose is to provide an assurance of monetary backup should the
Federek ever fall (not entirely unlikely, given the competitive nature
of the EnterCorps involved) but its practical use is mostly for those
times that you need to transfer money without the transfer being
tracked. If you know what I mean.


Social Life
-----------------

Social life? If I had a social life, you think I'd be writing up this
overview? Seriously though, the **Cyberlypse** doesn't really do a
social life. Not unless you're one of the chosen few: like a board
members of an EnterCorp, or a corrupt official, or you're romantically
involved with the right people or persons.

If you want to survive, you do one of two things. You work from 9 to 9
like everyone else, or you lay low. If you're laying low, you're
either a parasite or a criminal. Of course, being a criminal is a
broad term these days; I fixed my mother's printer the other day and
got fined Ḟ500 for it. It's almost enough to make me want to put on a
black hat and make a real living off my skills. Not that I'd ever do
that, of course.

Social lives do happen, of course. You meet people on the mesh, or
you meet them out on the streets. You do lunch. You get married. Your
kid grows up to be a company man or woman just like yourself. It's
fulfilling and rewarding. No, really. It is.


Language
------------------

Lots of languages, but the big three are English, Japanese, and
Esperanto. English and Japanese for obvious reasons; Esperanto because
the ISO made an attempt to make one language official so that all
Circuits could effectively communicate at long last. As such,
Esperanto is the official language, especially within the EnterCorp
and CityCorp (especially the ones that think they're EnterCorp)
sectors.

.. note::
   If world-building, and you need words or names for things, you
   can't go wrong by pilfering from Esperanto. It's the official
   language of the **Cyberlypse** so it's never out of place.

   A good resource for high-tech Esperanto lingvo is `Komputeko
   <http://komputeko.net/index_en.php>`_

   
Nation States
-----------------

There are more nations than you think, and more than the EnterCorp willingly recognises.

As an example, each of the 50 states of what was the United States of
America became its own nation after Y2K, not necessarily by concious
choice but because there was just no pragmatic way of coordinating
with a central authority to do the "united" part of the "USA".

Those 50 new nations eventually ended up, almost invariably, as at
least 2 new nations from each, and sometimes more. Translation: at
least 185 nations within the territory that used to be know as the
USA. And that's one example.

In practise, people don't really talk about "nations" or "states" as
much as they refer to "regions" and "territories". These sometimes get
drawn onto maps and labelled, but if you visit and ask around, most
people wouldn't even know about that. People live in their EnterCorp,
or their CityCorp, or their town. It's a lot more pragmatic than it
used to be.



Religion & Metaphysics
--------------------------

The religion in the **Cyberlypse** is, quite literally, the :ref:`Federek
<money>`_, but that's deliberately and explicitly a non-supernatural
religion. You please God by working hard, and God rewards you with
Federek. End of story. 

But of course that's just the average citizen. And I feel that you, my
friend, are far from average.

To paraphrase Arthur C. Clarke: if technology is sufficiently
misunderstood, it becomes *magic*. That's metaphysics in a
**Cyberlyptic** nutshell.

BioCode (``BioAssembly``, in ISO lingvo) is good enough at this point
that those that know it can "grant" powers to humans that would have had
entire cities burned at the stake in centuries past. "Summoning"
elements, "conjuring" familiars, moving physical objects by
"telekinesis". It's all here, and none of it mystical in spite of how
badly some people want to believe it is.

What *is* mystical is why anyone would ever subject themselves to the
process. It's not easy, and it's far from safe. Your normal un-altered
body can't use these "metaphysical" powers, so you have to get the
Cybergenetic upgrades, and then you have to have them programmed, and
you really really hope you get a programmer who knows how to grant
your fingers the ability to emit lightning bolts without also frying
you to a crisp.

But it's all done with solid, sound, scientific tech. Officially, there is
no spiritual realm in **Cyberlyptic**. In some regions, it's even
illegal to profess belief in the supernatural.

Of course, people being people, not everyone agrees with the official
decision that spirituality is obsolete. And *that* runs the gamut;
you've got fast-talking free-wheeling soapboxers who'll do and say
anything for a rek, and mystic voodoo spiritualists who freak even me
out with some of the things they can do (I'm sure it's all smoke and
mirrors, right? ...Right?), and truly penitent self-sacrificing people of
one of many folk gods (some old, some new, some reinvented). Take your
pick, you have plenty to choose from, and I'm not one to judge.

