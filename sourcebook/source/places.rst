Places
=============

Let's take it from the top down.

World
--------

It's the planet Earth. You're familiar with it. Third from the star
*Sol*.

Another nutjob is born every minute on this planet, so believe me when
I tell you that there are those who will try to convince you that
beings from other planets walk among us. Or that lizard people are
taking over the EnterCorps. Or that the wealthy have paradise colonies
on duplicate Earths way out in other galaxies.

OK, well actually that last one is partly true; the wealthy really do have
colonies on Mars. Not a big deal, and not a duplicate Earth, at least
from what I've been told (I've never been, obviously). Mars is
supposed to be a nice place to visit, and it's up-and-coming, but you
still have to stay inside biodomes, and it's a rich person's
playground. None of us are ever going to get there, so forget it
exists; doesn't concern people like us.

Aliens and lizard people, on the other hand, are not what you should
expect to find should you ever get a job catering an EnterCorp
party. The wealthy may seem alien, but mainly they're just
sludgewalkers in expensive gowns.

I can guarantee you, 100%, there are no aliens or lizard people on
Planet Earth. (Then again, if I were an alien or a reptilian, I would
say that, wouldn't I?)


Territories
------------

If you got two separate worldmaps from two different people, you can
almost bet real fed that they'll be different. That's because the
boundaries of the territories as seen by the people living in them
differ from the boundaries as seen by people living in neighbouring
territories, and both of those differ from what the nearest EnterCorp
sees.

As such, there's not that much of a point in talking about nations and
countries or even states. They just don't exist in a consistent way.

Instead, most people refer to either EnterCorps or territories, with
some allowance for hybrids, like CityCorps or towns.



EnterCorp
--------------

An EnterCorp is a citywide enterprise managed by a corporation.

Assuming you're an Outlander and have never been to an EnterCorp
before, you can think of an EnterCorp like a university
campus. There's housing, and dorms for the lower social strata, there
are office buildings, shops, and a little obligatory
recreation. Difference is, a university campus takes up maybe a couple
of city blocks; an EnterCorp *is* a city.

Now multiply that by 100, or 200, or 1000, depending on the company.

After Y2K, corporations embraced the realisation that they didn't need
a *product* to be a profitable corporation. They just needed buy-in
from a population. As long as someone builds the factory, people will
run it, and the running of the factory generates its own economy.

There's a lot less production happening than there are
corporations. The status quo creates itself, and sustains
itself. People are born in EnterCorp medic centers, they get educated
in EnterCorp schools, they go to work using EnterCorp transportation,
they go home to EnterCorp housing, they shop at EnterCorp shops, they
play EnterCorp games, they put their money onto EnterCorp mainframes,
they take EnterCorp medicine, and that's the **Cyberlypse** in a
nutshell.

Make no mistake: the EnterCorps are paradises. At least, they are
according to their citizens. You can't tell an EnterCorp dweller that
there's anything worth while on the outside. You can't tell them
there's more to life than making rek and praying against all hope for
a promotion. To them, the most important social causes exist in the
EnterCorp, and the only reason the world's as screwy as it is, is
because not everyone works for rek, and no everyone belongs to an
EnterCorp. Worse yet, if you so much as try to convince them
otherwise, you'll be branded as a destructive personality, a promoter
of pointless chaos, selfish, disruptive, apathetic, mean-spirited. You
are less than a penniless EnterCorp reject, you're a hopeless and
helpless waste of flesh unable to function within a sane society like
their EnterCorp.


Good Neighbours
^^^^^^^^^^^^^^^^^^^

If given the option, any given EnterCorp will essentially ignore that
any other EnterCorp exists. Of course, that's not usually an option in
the real world, whether for territory disputes or import and export
trade agreements, EnterCorps do have to interact sometimes, but it's
only ever for business transactions and it's usually avoided when
possible.

What this means to you and me:

* Lots of redundancy and inefficiency; there aren't global records
  being kept about anything aside from stock prices and rek
  valuation. This might be inconvenient to you if you're on a business
  trip to a neighbouring EnterCorp and go into a medic center only to
  find that they have no way of obtaining your medical records (I
  guess you should have upgraded to that Biogenetic medical chip after
  all?). On the other hand, it must be nice for a criminal to escape
  one EnterCorp and wander into another with a clean slate (not that I
  would know).
* Federek valuation is mostly the same across all EnterCorps because
  it's the one technology and philosophy all EnterCorps agree
  on. Natural resources, obviously, are not considered equal, but it
  all manages to work its self out in the end. Blessings are upon
  those who buy and sell wisely.
* Meshes are local entities, but meshes can talk to meshes, so if you
  try hard enough, you can get into a mesh within a neighbouring
  EnterCorp, but there isn't usually a point to it. The meshes of
  other EnterCorps are boring, because they talk about things in that
  EnterCorp, people and celebrities who mean nothing to you or your
  friends, business deals that don't touch you, and politics that
  don't affect you. So, yes it's done, but not widely. On the other
  hand, getting onto neighbouring meshes in order to discover trade
  secrets, upcoming business moves, shortages, and other privileged
  information is an art and a business all its own, and a quite a good
  one to be in, if you have a talent for it.


Ditch
--------------

An EnterCorp, by nature, wants to expand. It has no limitation, since
its only purpose is to add to its own wealth, and wealth is, whether
anyone admits to it or not, measured in people.

As such, the people who, for whatever reason, aren't useful to an
EnterCorp get pushed out to the fringes, right at that special, hazy
tranisition of EnterCorp and what is technically called the "belt" but
is better known as *the ditch*.

The ditch are the outer outer suburbs around an EnterCorp, the
industrialised fallout living in the shadows of a city too big to care
about it, but too greedy to let it go.

An EnterCorp's ditch can be big and sprawling, or it can be small and
isolated; it just depends on the brand of apathy its parent EnterCorp
prefers. The ditch is home to many, but it's home *sweet* home to an
outlander needing to "do business" with an EnterCorp without a formal
invitation. It's well close enough to the EnterCorp to allow for easy
and native mesh access, or even daily excursions into the heart of the
EnterCorp if you enjoy CosPlay, but far enough out of mind and site to
allow for a reasonable hope of privacy.

Sadly, the ditch ain't cheap. You'd think it would be, and it *is*,
compared to EnterCorp rent, but if you have no Federek income, then
it's not exactly inviting. You might be able to skate by on blit, but
don't go in banking on that. Ditch citizens can vary from Outland
sympathisers to down-and-out EnterCorp citizens who would love nothing
better than to turn a trespassing Outlander over to EnterSec and
collect the cursory reward.

Squatting in the ditch is also an option, as long as you find a big
enough EnterCorp with a really properly destitute belt. You'll have
less luck with the micro-managing EnterCorps, or any given EnterCorp
with a fresh CSO (Chief Security Officer); they tend to scrub the
ditch regularly in an attempt to flush out the bad elements.

Occasionally an EnterCorp gets the itch to expand. That means the
ditch gets gentrified, proud CitizenCorp moves in and start bouncing
new families, and everything's peachy...right up until a new ditch
forms along *its* out border, and the circle of life is complete.


Outlands
--------------

The "Outland" is, simply put, anything not owned by an
EnterCorp. More precisely, it's anything not actively *in use* by an
EnterCorp, since everything is, at least if you ask any given EnterCorp,
"owned" by an EnterCorp.

To people in an EnterCorp, the very existence of the Outlands is an
embarrassment. To Corp enangelists, it's a sin that the poor,
misguided inhabitants are left out of the glory of the holy Federek,
to social activists, it's a crime that the Outlanders don't have the
same opportunity to earn Federek of their own, to upper management
it's a budgetary shortcoming that Outlanders are not working for them,
and so on.

To an Outlander, on the other hand, life in the Outlands is exactly
what real life is meant to be; free, rugged, independent. To them,
it's the EnterCorps that are the blemishes on the human race.

And that's roughly the typical relationship between the two extremes;
EnterCorp people see the Outlanders as backward redneck types,
bandits, or uncivilised savages (depending on who you ask), and
Outlanders see the EnterCorps folk as helpless drones toiling away in
an evil and emotionless factory. Me? I think, like most extremes,
there's truth to both sides.

The fact is, while a CorpCitizen usually sees their EnterCorp as the most
important part of the planet, the EnterCorps are mere islands in
oceans of Outlands. Roughly 75% of the planet's *populated land* is
"outland", leaving the rest to EnterCorp developments.


Welcome to the Outlands
^^^^^^^^^^^^^^^^^^^^^^^^^^

The Outland is a diverse place. Without a centralised government, the
Outlands are filled with settlements that formed from the total
communication breakdown of Y2K. Laws, such as they are, formed around
whatever worked best for the local population.

Between settlements is more Outland, and that usually tends to be a
little rougher around the edges. In those spaces in between
civilisation, even makeshift laws never formed, so anything can
happen.

The laws that do exist in Outland settlements fall into three
categories:

* No laws. Total anarchy, not necessarily a negative connotation
  (also, not necessarily *not* not a negative connotation). There are
  areas where a deliberate lack of law is the "law" of the land. It
  works, and it works well. People keep to themselves, or they live
  together in a commune, or they live in micro societies (tribes), and
  they interact with one another based on common needs. Other times,
  of course, it doesn't work so well, because there's only anarchy
  because no one ever thought to talk one another about how they'd
  co-habitate in an area, and so it's every Outlander for him or
  herself.
* GPL (the meaning of this acronym has been lost to time, but it is
  generally accepted to assumed to mean something like "General Public
  License"). Under this law, resources in a society are shared with
  you as long as you share your resources back, according to whatever
  "resource library" system a settlement has set in place (it usually
  gets measured in :ref:`blit`_, since currency is basically
  meaningless in the Outlands). 
* BSD (acronym lost, but everyone settles on "Benefitting Society by
  Donation", or something similar). This is a donation model; no
  :ref:`blit`_ is exchanged. A resource is simply a donation on a first-come
  first-serve basis. Of course, the human race being the human race, there are
  always Outlanders who take BSD resources, lay a claim to them, and then sell
  them for their own gain; strictly speaking, that's acceptable,
  even if it's not quite in the spirit of the original donation.

There are many variations, but these three general ethical pillars are
fairly universal throughout the Outland, coming as they did from the
:ref:`commies`_ back when the Outlands were flailing in desperate tech
deprivation and were saved by the techno-smart commies and their high
and mighty ideals of no centralised governance, sharing for the common
good, and staunch independence.

The bottom line is: for as many settlements that you visit in the
Outland, that's how many different social structures you'll find. To
the rigid minds of CorpCitizens, it sounds like chaos. To Outlanders,
it's exactly what makes the Outland great.



RuralCorp
^^^^^^^^^^^^^^^^^^

In some cases, an EnterCorp does find it beneficial to open a
satellite Corp outside EnterCorp limits. When this happens, they clear
out a lucrative area of the Outlands and set up shop.

These satellite Corps are usually either farming or mining hubs;
everything else, an EnterCorp can usually generate within the
EnterCorp, but areas rich in some natural resource is just too good
for a nearby EnterCorp to pass up.

The CorpCitizens inhabiting a satellite Corp can vary widely; you'll
find diehard Corp citizens who see their foray into the great wide
world as missionary work, and then you see citizens secretly wanting
to be an Outlander.

By necessity, these satellite office-towns are usually open to the
idea of Outlanders and Outlander lifestyle. They kind of have to be;
they're living it, or at least the posh version of it. They don't
always *like* interacting with Outlanders, but it's not something they
can avoid.

Most Outlanders see RuralCorps as convenient evils. They're nice
places to visit for supplies you just can't find in the Outlands under
normal circumstances, but it's still the EnterCorp.


Outland Meshes
^^^^^^^^^^^^^^^^

Mesh connectivity in the Outland is not guaranteed since many
settlements are spread far away from one another, and not all
settlements have a stable electrical grid. However, most people can at
least drive a radio, so in a pinch there's a radio mesh throughout the
Outlands (and, being radio, even within the EnterCorp if you think to
listen for it and as long as the EnterCorp isn't actively overpowering
it).

It's a completely different mesh style, but basically the same
needs are met; information can be shared, information can be stolen,
connections are made. It's a mesh, but a hacker on an EnterCorp mesh
is going to find the Outland mesh about as familiar and comforting as
he or she would find the actual Outlands, and the same is true for an
Outland hacker on an EnterCorp mesh. The protocols are different, the
programs that run are different; learning one is not learning the
other, so hackers have to choose their skills according to which mesh
they actually intend to interface with regularly. 

The main difference between the two meshes are physical
locations. Within an EnterCorp, mesh addresses correspond to building,
which obviously do not move, or devices attached to people, whose
every move is tracked. If you're trying to correlate a mesh entity
with its realworld counterpart, it's fairly easy in an EnterCorp
because everything is regulated by the Corp.

In the Outlands, the radio mesh is more like SONAR; there are some
fixed points (like radio towers and repeater stations) but the signals
from people and devices are prone to move around without notice, and
without being tracked. If they communicate again, then you can usually
calculate their location based on their previous broadcast (assuming
they didn't use a repeater to broadcast to the mesh), but all in all
it's a lot harder to get to real people via the mesh in the Outlands.




Forbidden Zone
---------------

I have no idea what is in the forbidden zone.

No, I literally don't. No one's ever been, or if they have, they
either don't admit to it, or they don't come back.

Stories say that the forbidden zones are hot with radioactivity;
meltdowns invariably caused by Y2K computer crashes. Could be true,
could be a story the CEOs tell us to keep us out of their super-secret
nuclear-powered private resorts. Your guess is as good as mine.
