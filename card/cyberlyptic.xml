<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook V5.0//EN" "/usr/share/xml/docbook5/schema/dtd/5.0/docbook.dtd">
<article>
<info>
  <abstract>
      <para>
	Rules for the solo game or co-op cyberpunk dungeon-crowler
	card game, <productname>Cyberlyptic</productname>, developed
	by Klaatu.
      </para>
    </abstract>

    <title>Cyberlyptic</title>
    <copyright>
      <year>2017</year>
      <holder>
	Creative Commons Attribution-ShareAlike 4.0 International
      </holder>
    </copyright>
	
  <author><personname>
    <firstname>Klaatu</firstname>
  </personname></author>
  <volumenum>1</volumenum>
  <issuenum>1</issuenum>
  <publisher><publishername>Fat Chance Publisher</publishername></publisher>
  <pubdate>2017</pubdate>
  <revhistory>
    <revision>
        <revnumber>0.5</revnumber>
        <date>2016-07-30</date>
        <revremark>Started plans for cyberpunk-themed location-based
	RPG-like card game.</revremark>
     </revision>
     <revision>
        <revnumber>1.0</revnumber>
        <date>2017-02-01</date>
        <revremark>Brisbane. Late nights. First version happened.</revremark>
     </revision>
  </revhistory>

</info>

<section xml:id="requirements">
  <title>Requirements</title>

  <para>
    To play Cyberlyptic, you need&#58;
  </para> 

  <itemizedlist>
    <listitem>
      <para>
	At least one set of Cyberlyptic cards.
      </para>
    </listitem>

    <listitem>
      <para>
	At least 2d6 &#40;two 6-sided dice&#41;, or a d6 and a d12. In
	a pinch, almost any dice will do, as long as you're
	flexible. If you have no dice, use pen and paper to track
	stats, and <link xlink:href="http://www.rpgnow.com/product/195037/Pocket-Dice-Roller">Pocket
	Dice Roller</link> for dice.
      </para>
    </listitem>

    <listitem>
      <para>
	A miniature or token to represent your hero. In a pinch, you
	can use coins, paperclips, or whatever you have lying around.
      </para>
    </listitem>

    <listitem>
      <para>
	Tokens to represent various elements in the game, such as
	drones, random items, or escorted companions.
      </para>
    </listitem>
  </itemizedlist>    
</section>    

  <section xml:id="table">
      <title>Game Table Setup</title>

    <orderedlist>
      <listitem>
	<para>
	  Divide cards into separate decks, based on the backs of the
	  cards. You should initially end up with 4 decks: Location cards,
	  Character cards, Mission cards, and Encounter cards.
	</para>

	<para>
	  Take the <emphasis>Encounters</emphasis> card deck, and
	  separate the cards in it into two more decks&#58; the Posix
	  deck &#40;the positive cards bearing the plus &#40;+&#41;
	  icon in the upper corner&#41; and the Negex deck &#40;bad
	  cards, bearing a minus &#40;-&#41; icon in the
	  corner&#41;. You now have a total of 5 decks of cards.
	</para>
      </listitem>

      <listitem>
	<para>
	  Place these decks, in this order, along the top of your game
	  table: Location cards, Special cards &#40;if any exit&#41;, Posix deck &#40;with the artwork right-side up from your
	  perspective&#41;, Negex &#40;with the artwork is upside-down
	  from your perspective&#41;.
	</para>

	<!-- mediaobject>
	<imageobject>
	  <imagedata fileref="images/solo-layout.eps" format="EPS"/>
	</imageobject>
      </mediaobject -->

	<para>
	  Set the Character deck aside. It isn&#39;t used during
	  gameplay, but holds preset playable characters.
	</para>
      </listitem>
      
      <listitem>
	<para>
	  Look through the Location cards and find the card marked
	  <emphasis>Start</emphasis>. Place the Start card in the
	  center of your game table.
	</para>
      </listitem>

      <listitem>
	<para> Draw 4 more Location cards randomly and place them
	around the entrance. If there&#39;s CAUTION tape along an edge
	of a Location, then entrance is blocked. Make sure a location
	is not entirely blocked off when you place it on the map, or
	it will be entirely inaccessible&#33;
	</para>
      </listitem>

      <listitem>
        <para>
	  Pick one character from the Character deck and set the
	  remaining character cards aside. If your first character
	  perishes, you can choose a secondary character, so keep it
	  close by.
	</para>

	<para>
	  Place your character card face up in front of you. To your
	  character card's left, you'll place learnt abilities. This
	  is your random access memory bank. To your character card's
	  right, you'll place your current hand.
	</para>

	<para>
	  Use a single 10-sided die &#40;d10&#41; to represent your
	  character&#39;s health. Start with 10 health &#40;usually
	  represented as 0, facing up&#41;. You don't roll this die, it's just a
	  counter. For a more difficult game, use a d6 instead. For an
	  impossible game, use a d4.
	</para>
      </listitem>

      <listitem>
	<para>
	  Pick 2 or 3 Missions, depending on your desired difficulty and
	  game length. Place them face up on the game table. These are
	  your goals for the game.
	</para>
      </listitem>
    </orderedlist>
    </section>
  
  <section xml:id="play">
    <title>Game Play</title>

    <para>
      Each round moves from left to right along the decks at the top
      of the table. First, draw a Location card and place it in a
      valid space on your table. A valid space is adjacent to
      any card such that your new location is reachable from at least
      one direction.
    </para>

    <para>
      Next, process any Special cards.
    </para>
    
    <para>
      On your turn, you have as many actions &#40;AP&#41; as your
      movement points. For each move, you may&#58;
    </para>

    <orderedlist>
      <listitem>
	<para>
	  <emphasis>Travel&#58;</emphasis> physically move your player
	  token to adjacent locations. Moving to a new location costs
	  1 AP, just as movement would in a typical board game.
	</para>
	<para>
	  In an attempt to force you to make this game more of an RPG
	  than most solo RPG games naturally are, you can regain 1 AP
	  <emphasis>per turn</emphasis> by stating aloud one clue, or
	  one attribute, that your character finds in the new map
	  location, even if they've been there before. You must use at
	  least three sentences. For example&#58; &#34;Krug enters the
	  Abandoned Subway Station and sees weathered markings on the
	  wall. He looks closer. It&#39;s his surname. Could it be
	  that his ancestors trod these tunnels before?&#34;
	</para>
	<para>
	  If you do not narrate your Location move, then do not regain
	  1 AP.
	</para>
      </listitem>

      <listitem>
	<para>
	  <emphasis>Learn&#58;</emphasis> if a card has a floppy disk
	  icon on it, then it is a savable skill. You can transfer a
	  savable card from your hand to your learned abilities by
	  placing an ability to the left of your Character card. Cards
	  not marked with a disk icon cannot be learned. Your learned
	  bank can only contain 3 cards total, so discard back down to
	  3 as necessary.
	</para>

	<para>
	  Your limit for learning, per action, is the plus &#40;+&#41;
	  icon in the top right of your current Location card. If your
	  current Location is marked <parameter>1</parameter>, then
	  you can only move one card to your learned memory bank. To
	  learn another programme or skill, you must use another move
	  point.
	</para>

	<para>
	  If, however, your current Location is
	  marked with a <parameter>3</parameter>, then you can transfer
	  3 cards to your pack by spending just 1 AP.
	</para>
	
	<note>
	  <para>
	    You cannot move a card from your learn bank back into your
	    hand, so moving a card to your memory bank is effectively
	    moving it one step closer to the discard pile. If you have
	    three cards in your bank and add a fourth, then some card from
	    your memory bank must be discarded.
	  </para>
	  <para>
	    Then again, moving a card into your memory bank does make more
	    room in your hand for new cards.
	  </para>
	</note>
      </listitem>

      <listitem>
	<para>
	  <emphasis>Explore&#58;</emphasis> draw a card from the Posix
	  deck. You can only have 3 cards in your active hand, so if
	  you keep what you draw, you must discard down to 3 total
	  &#40;or use another action to Learn an ability so that you
	  transfer a card from your hand to your memory bank&#41;.
	</para>
      </listitem>

      <listitem>
	<para>
	  Play as many Posix cards as allowed by your current
	  Location. For example, any card marked as an
	  <emphasis>Start</emphasis> card has a
	  <parameter>1</parameter> over a plus &#40;+&#41; icon in the
	  top right corner. This means you may play
	  one Posix card from your hand. On a map
	  location marked <parameter>3</parameter>, you're
	  allowed to play three Posix cards.
	</para>

	<note>
	  <para>
	    Usually, you only play Posix cards to help you in a fight,
	    but sometimes you might need to play healing Posix cards
	    out of combat.
	  </para>
	</note>

	<para>
	  At the end of a round during playing a Posix card, you
	  discard the Posix cards in play, <emphasis>unless</emphasis>
	  they are stored in your memory bank.
	</para>
      </listitem>      
    </orderedlist>

    <para>
      The final stage of each round is to resolve Negex cards. Negex
      cards are played only if you travelled by foot during this
      round. If you used all of your move actions to manage cards or
      heal, or if you got transferred into a location over the network
      or some other action, then no Negex card is played.
    </para>

    <para>
      If that is the case, this round is over. Reset your cards
      &#40;discard any Posix cards you used, unless they are in your
      memory bank&#41;. Begin the
      cycle again by drawing a new Location card &#40;back up at the top of
      this <xref linkend="play"/> section&#41;.
    </para>

    <para>
      Assuming you did travel by foot to a new map location, then the
      round is not yet over. You are now in <xref linkend="combat"/>.
    </para>
    
    <section xml:id="combat">
      <title>Encounter</title>

      <para>
	On your new map location, look at the number over the red
	minus &#40;-&#41; icon in the top corner. This is the Negex
	rating of that location. During any given round, you have the
	option to treat this rating in one of two ways&#58;
      </para>

      <itemizedlist>
	<listitem>
	  <para>
	    Draw 1 Negex card and grant it a bonus of the total Peril
	    rating. For instance, if you're in a map location with
	    3 Peril, draw 1 card and grant it 2 bonus on all attacks
	  and speed.</para>
	  <para>
	  You can make this decision as you draw. For instance, if you
	  draw a relatively low-level thug and decide you&#39;d rather
	  grant him bonus power-ups than draw two more cards, you can
	  choose to do that. It's not cheating to make the call as you
	  draw.
	  </para>
	</listitem>

	<listitem>
	  <para>
	    Draw the same number of Negex cards as the Negex rating,
	    and resolve each one in turn. For example, if your map
	    location is 3 Negex, draw 3 Negex cards.
	  </para>
	</listitem>
      </itemizedlist>

      <note>
	<para> On any location with a Negex rating of 2 or
	greater, you can ignore 1 Negex point if you speak aloud
	at least three sentences about the encounter. For
	example&#58; &#34;Just then, a hover car lands, narrowly
	missing Krug by just a meter. From the car leaps a Ret
	Thug, with more cyber-enhancements than he could probably
	afford, and a gun as big as his arm. No wait, that is his
	arm. Krug smiles and cracks his knuckles. He&#39;d just
	been wondering where all the action was hiding.&#34;
	</para>
	<para>
	  If you do not narrate an introduction for the encounter,
	  do not ignore a Negex point.
	</para>
      </note>
      
      <para>
	Resolve special effect cards first &#40;in this order&#58; Viruses,
	Traps, and then Opponents&#41;.
      </para>

      <para>
	Prior to the first strike, you may activate Posix cards in
	your hand &#40;you don&#39;t have to activate cards that are
	in your memory bank, because if they&#39;re in your memory
	bank, they already apply to you anyway&#41; <emphasis>up
	to</emphasis> the number of cards in your current location's
	green plus &#40;+&#41; number &#40;not counting memory bank
	cards&#41;. After combat is over, any Posix cards you used
	during this round are discarded.
      </para>

      <para>
	Combat is performed with a d12 or 2d6. Results are Boolean&#58;
	either you, or your enemy, win the roll + bonuses. The loser
	endures 1 Damage. If you lost, decrement your health die. For
	your opponent, either keep track of their health mentally or use
	a d4 or spare d6 on their card.
      </para>

      <para>
	Combat lasts for as long as it needs to last for you to kill
	your opponent or opponents, or until you are killed, or until
	you <xref linkend="escape"/>.
      </para>

      <para>
	When you win at combat, do not place your enemy at the bottom
	of the Negex deck. That enemy is gone and cannot return. Place
	it back in the box. If you run out of Negex cards, then you
	have secured the area and you have won the game &#40;even if
	you fail your Missions&#41;.
      </para>
    </section>

    <section xml:id="escape">
      <title>Escape</title>

      <para>
	Escape is just like combat, except you&#39;re rolling against
	your enemy&#39;s AP. If your roll + bonuses is greater than
	your enemy's roll + bonuses, then no damage is dealt to either
	of you, and you&#39;re able to move out of the current map
	location into an adjacent space. Leave the opponent card
	&#40;or a token&#41; on that map location. If you return to
	that spot, you must fight any opponents inhabiting it
	immediately upon entering.
      </para>

      <para>
	If you lose the roll, you endure 1 Damage. Decrement your
	health die. You do not escape. You can keep trying to escape,
	but for each failed attempt, you endure 1 Damage because you
	were unable to evade your opponent&#39;s
	attack-of-opportunity.
      </para>
    </section>

    <section xml:id="win">
      <title>Winning</title>

      <para>
	To win the game, you must&#58;
      </para>

    <orderedlist>
      <listitem>
	<para>
	  Complete all missions you placed on the table.
	</para>
      </listitem>

      <listitem>
	<para>
	  Return to a Start card.
	</para>
      </listitem>
    </orderedlist>

    <para>
      Alternately, when you run out of Negex cards, you may assume you
      have secured the area, and whether or not you&#39;ve completed
      your Missions, you have successfully dominated your turf.
    </para>
    
    <para>
      You may declare defeat if you are well and truly dead &#40;not just Mostly
      Dead&#41;. 
    </para>
    
    <para>
      However, you don&#39;t have to declare defeat if you don&#39;t want
      to. All characters in this game have a deadman&#39;s switch&#59;
      if you lose combat so thoroughly that you run out of health
      points, then that character is dead, but a new character can
      seek revenge. Place your dead character card on the Location
      card where they die, and pick a new card from the Character deck. Continue the
      game&#39;s story by introducing this new character at the
      <emphasis>Entrance</emphasis> location.
    </para>

    <note>
      <para>
	Gain 2 bonus AP for your first turn if you speak aloud at
	least three sentences why your new character is taking over in
	your old character&#39;s stead. Maybe they were friends? maybe
	someone hired your new character? if so, why, and whom? 
      </para>
      <para>
	If you do not narrate the transition in story, do not take 2
	bonus AP points.
      </para>
    </note>
    
    <para>
      When your new hero enters the previous character&#39;s death
      space, your new hero must immediately fight the malicious,
      positronic-zombie version of your previous character, including
      their learned pack bonuses. When your previous character is
      defeated, their soul is set free on the network, and they
      proceed to a nice, peaceful afterlife as an avatar.
    </para>

    </section>
  
    <section xml:id="trap">
      <title>Obstacles and Traps</title>

      <para>
	Some locations have traps lying in wait for anyone who enters.
      </para>
      
      <para>
	Traps and other obstacles are treated exactly like
	<xref linkend="combat" />, except that instead of rolling die and
	adding your character&#39;s melee or hacking rating to your roll, you add
	your <emphasis>AP</emphasis> rating.
      </para>
      
      <para>
	For example&#58;
      </para>
      
      <para>
	You&#39;re a level 1 hacker with an AP rating of 2.
      </para>
      
      <para>
	You enter a room with a trap with a rating of 6.
      </para>
      
      <para>
	You roll a 2 with your die, and add your AP (2) to that for a total
	of 4. You lost&#33; You make it into the location, but you suffer
	whatever consequence the trap specifies on the card.
      </para>
      
      <para>
	If you fail a test against a lock, you are not able to enter that
	location. There may or may not be additional consequences listed on
	the card.
      </para>
    </section>

    <section xml:id="notes">
      <title>Notes</title>

      <variablelist>
	<varlistentry>
	  <term>Virus</term>
	  <listitem>
	    <para>
	      To determine which direction a virus, such as a Scramble,
	      sends you, perform a <emphasis>Cardinal Roll</emphasis>. A
	      Cardinal Roll uses a d6&#58; North=2, East=3, South=4,
	      West=5. Reroll on 1 or 6.
	    </para>

	    <para>
	      A simpler and faster option is to just make a choice in the
	      moment.
	    </para>
	  </listitem>
	</varlistentry>
	
	<varlistentry>
	  <term>Awareness</term>
	  <listitem>
	    <para>
	      Awareness is a check to see if you were caught
	      off-guard. For instance, if an enemy Encounter card
	      states &#34;if Hero is caught off-guard, then melee
	      deals 2 Damage&#34;, then before entering combat, roll
	      an Awareness check with a d6. If you beat your
	      opponent&#39;s Stealth rating, then you are not caught
	      off-guard. If you fail the check, then your enemy deals
	      2 Damage instead of 1 during combat.
	    </para>
	    <para>
	      If your Hero is resistant to surprise attacks &#40;it
	      will say so on your Character card, if you are&#41;, or
	      if you have a Posix card in play that makes you immune
	      to surprise attacks during the current round, then you
	      are exempt from having to do an Awareness check.
	    </para>
	  </listitem>
	</varlistentry>
      </variablelist>

    </section>
  </section>

</article>
