Things
=============

Tech
--------

The future was supposed to bring flying cars and holograms. Lucky for
the **Cyberlypse**, the future never came, so it never had to deliver
on that promise.

The biggest achievement for technology in the **Cyberlypse** is
*maintenance*. Technicians in the **Cyberlypse** have, more
successfully than anyone before, taken then remnants of old technology
and learned how to keep it running, come hell or high water (and,
depending on what part of the **Cyberlypse** you live in, you may have
experienced both of those things, too).

The reason technology has came to a standstill at Y2K is because the
biggest manufacturing plants of the time fell apart. Microchip
manufacturing became a moot point when there were more important
things to worry about, like getting your population food and clean
water, and rebuilding society in general. By the time people pulled
themselves together again, the secrets of silicon dye and CPU
manufacturing had faded, and the factories that used to produce them
had crumbled.

But then again, so had the priorities of the world. People were less
concerned about making new junk now that they understood that they
were going to be the ones to sort through it and take it to the
landfill when it was all used up. The wisdom of using something until
it was used up, and then repairing it, and then using it for another
20 years, had returned to modern society. In the **Cyberlypse**, you
get one computer, and that's your computer for life. You might trade
parts with friends, you might repair it when it dies, but that's your
computer until either it or you can't compute any more.

As such, the technology in the **Cyberlypse** is actually really old,
and it's usually big, unless a very clever hacker has taken it apart
and re-packaged it into something smaller.

Don't let that fool you, though. This is powerful
technology. Necessity has demanded programmers to write smarter code,
and faster code, so that they can still develop even though the
processors aren't getting any faster. And of course you can overclock
and hack hardware, but even without those sorts of mods, the geeks
have gotten cleverer and are doing exciting new things with ancient tech.


Hardware
^^^^^^^^^^^^^^

This is not an exhaustive list, but these are some of the typical rigs
you'll see around the **Cyberlypse**.

Some of the popular hardware may or may not seem familiar to you,
although most old "brand names" have long since been replaced by the
names of the EnterCorp, hacker group, or random geek who revived and
modded it:

Citizen Hardware
`````````````````
.. csv-table:: RPG Traits
   "Damage", "1d6"
   "BogoMips", "2(e3)"
   "Conspicuity", "1-2"

Normal hardware includes whatever a normal citizen can get hold of for
their own entertainment. Might be a **PDA Pilot** ("Personal Data Assistant", a
small touch-tablet that fits into the pocket), a hefty calculator
(such as the popular **Titanium-81**), or any variety of laptop
computers. These devices are generally dumbed down so that they do the
routine things a normal Corp Citizen needs; transfer Federek from
account to account, process work documents and data, send messages,
access consumer-appropriate mesh sites.
   
Corp Gear
```````````
.. csv-table:: RPG Traits
   "Damage", "1d6"
   "BogoMips", "3(e3)"
   "Conspicuity", "3-5"

   
Modded and Flat-out Banned
`````````````````````````````

.. csv-table:: RPG Traits
   "Damage", "2d6"
   "BogoMips", "4(e3)"
   "Conspicuity", "6+"

   
Software
^^^^^^^^^^^^^^^^^^

Gearwear
^^^^^^^^^^^^^

Cybergenetics
------------------


Vehicles
-------------


Food
-------------


Cameras and Video
---------------------


Audio
--------------


Tools, Weapons
----------------------
