#!/usr/bin/python3
# gplv3 klaatu
# this is concept art for my idea of a boolean die.
# the idea is that you roll a die and it decides YES or NO
# as to whether you achieve the goal. You also roll a
# traditional d6 to determine how well or how poorly you
# succeed or fail.

import random 
import sys

PLAYER=8
TALENT=2

#curses are always even numbers
try:
        CURSE=int(sys.argv[1])
except:
        CURSE=0

def booldie():
        db=random.randrange(1,6)
        return db

def infdie():
        di=random.randrange(1,6)
        return di

#turn
DB = int(booldie()%2)
print("boolean die=", DB)
DI = infdie()
print("influence=", DI)

#aftrmath
if DB%2==0:
    #miss,0
    if DI==2:
        print("No damage, but enemy is confused.")
        print(DI-CURSE, " on enemy's next roll.")
    elif DI==4:
        print("No damage, but you recover quickly and are ready for the next attack.")
        print(DI-CURSE, " on enemy's next roll.")
    elif DI==6:
        print("No damage, but you knock the enemy off balance.")
        print(DI-CURSE, " on enemy's next roll.")
    else:
        print("A swing and a miss.")

else:
    #hit,1
    DMG=DI+DB
    # should curse be deducted from die or the DMG dealt?
    if DI==1:
        print("You hit and deal ", (DMG+PLAYER)-CURSE, " damage! Lucky shot!")
    elif DI==3:
        print("You hit and deal ", (DMG+PLAYER+TALENT)-CURSE, " damage! Nice one!")
    elif DI==5:
        print("You hit and deal ", (DMG+PLAYER+TALENT)-CURSE, " damage! Truly you are a skilled warrior.")

        #losing the next turn may be harsh.
        #mayhaps just deal DI curse back at the enemy?
        print("Enemy stumbles back in a desperate attempt to recover, and loses the next turn.")
    else:
        print("You hit and deal ", PLAYER, " damage!")
