
Non-Contiguous Canon
========================

The point of **Cyberlyptic** is to provide a baseline setting for
techno-fantasy stories, games, music, and anything else you can dream
up. It becomes utterly useless to you if you cannot customise, even to
a point beyond recognition, the universe for your own use.

On the other hand, how does **Cyberlyptic** grow and expand if every
time someone uses it, it's completely different?

The answer is a resounding "huh?"

Look, it's not **Cyberlyptic**'s mission to impose rules; its mission
is to develop a communal creative entity that we can all use. Said
another way, and more accurately: **Cyberlyptic** is in the business
of painting a background motif for you, with a strict set of rules for
you to ignore.

To preserve both its own usefulness and its own identity,
the **Cyberlyptic** canon is split into modules.


Overview
----------------

Wrap your mind around this: the **Cyberlyptic** canon is
non-contiguous. If I write a **Cyberlyptic** story and you write a
**Cyberlyptic** story, and I say that in 2045 the first Martian
colony is established, but *you* set my story in 2019 and already have a
Martian colony, *that's OK*. We both have our private universes, and
they never have to match up to one another.

In other words: **Cyberlyptic** can be rebooted any old time you
want. 

To prevent a universe that constantly reboots itself everytime it is
invoked, I maintain a **Cyberlyptic** core canon, with contributions
from anyone who cares to help with world building. World-building the
**Cyberlypse** will ideally happen as people use it, and not as a
separate activity; in other words, as people use the **Cyberlyptic**
universe in art, that art in turn fleshes out the backstory of the
**Cyberlyptic** universe.

So, assuming you *do* want to play along, here is how I am structuring
the verse:


Cyberlyptic Core
----------------------

You're using the Core canon if you don't directly negate anything
written in a **Cyberlyptic** handbook. For example, Core canon says
there is no actual *magic* (like, Merlin-style elemental magick, or
actual vampires or werewolves, and so on), so if your story or game or
whatever explores magic or the supernatural world and draws the
conclusion that there *is* magic, then you're not using Core. It's not
a "you have to do it my way or else" thing; the goal is that when
someone says there's a new PRG game that is set in the **Cyberlypse**,
you know right away what to expect.

Most importantly: this is all just for fun; it's not like we're
applying for citizenship here, so if you're unsure about something,
just do what you think makes most sense.

For example, what if your artwork *suggests* that there is magic, in a
kind of "was it or wasn't it?" kind of way? In my view, that would be
in line with Core, because no absolute conclusion is drawn, regardless
of how very strongly it is suggested.

If your project introduces completely new ideas that have not been
addressed yet in Core, then you're still using Core. If you feel you
have added a substantial amount to the basis of Core, then contribute
your updates! I'd love to integrate your world building effort into Core!
Secondly, if you feel that you have added a lot but that it doesn't
really fit into Core, then maybe you've created your own :ref:`Universe <universe>`_.

Realistically, I am imagining that Core will be the *least* used
model, because it is the one with all the rules in it. But that's its
purpose; to be the most **Cyberlyptic** it can possibly be. It's the
backdrop, not the story.

Merge Conflicts
^^^^^^^^^^^^^^^^^^^^^^^

So what happens when stories conflict? What if I write a story set in
2045 about the first Martian Colony, only to find out later that you
had already made a game set in 2019 in which someone visits a Mars
colony?

I propose a cascading set of optional solutions:

1. **First come, first canon.** If your story beats mine to the punch,
   then your canon wins official recognition. If at some point there
   is enough interest in revising the canon so it makes more internal
   sense, then it can be revised and released as the new edition; not
   a problem. If that happens and I don't like the revisions, I can
   stay on the old canon, and everyone wins.
2. **Context.** If one version of a canon story makes more sense than
   another, given the context of existing canon, then the one that
   "fits" best wins. If my story has a Mars colony in 2019 but it is
   was already established that space travel isn't resurrected until
   2033, then the first Mars Colony happening in 2045 makes a lot more
   sense than one in 2019.
3. **Conspiracy Theories.** Truth be told, the "truth" is pretty hard
   to nail down even in the real world, so why should it be any
   different in the **Cyberlypse**? "Sure, people will tell you there
   was no Martian Colony til 2045, but from what I hear, that's just
   what They *want* you to believe..." Yes, we can have conflicting
   truths in the same canon.
4. **Expanded Multiverse.** The strength of the **Cyberlypse** is that
   it is infinitely forkable. If you like your version of what you are
   building and you feel like canon is going in a direction you don't
   like, that's OK. Anyone and everyone always has the right to fork
   the universe and go down their own timeline. There will never be
   any hard feelings; this isn't just "ok", it's encouraged. The point
   of **Cyberlyptic** is to foster creativity, not create drama (at
   least, not the real life kind).


Cyberlyptic Universe
-----------------------

You're creating an entry in the **Cyberlyptic Universe** if your
project used **Cyberlyptic** as a setting and a basis for whatever it
is that you are creating, but you're not working on the Core
world-building effort. Nothing in your work goes *against* anything in
Core, but then again you're also not building core mythology.

That being said, any event created in the universe might be merged
(with your permission, of course) into Core, just so we can build and
maintain a sense of continuity, but that's an afterthought.

Ideally, this will be the most common model; people use
**Cyberlyptic** as a springboard, staying mostly within Core canon,
but ultimately creating their own personal canon. Really,
that's the intent of the entire project.


Cyberlyptic Multiverse
----------------------------

You're creating an **Multiverse** entry if your project negates
anything written in the Core canon.

I think of it this way:

Imagine you're out buying a comic book, and it has a little line at
the bottom that says *Cyberlyptic Universe* on it, and the guy at the
cash register says "I've heard of Cyberlyptic. That's that one
setting, where the world is ruled by tech corporations and hackers go
around killing people with BioAssembly code."

And you say "Oh, not this one. In this world, all that's the same,
except it takes place in the 1800s, and instead of microchip
technology everything's based on steam power." 

That's a multiverse entry.

Less drastic example: say that Core canon says space travel is revived
in 2033, and there is lots of obvious and public acknowlegement within
the universe that space travel is a thing. We can't hedge around it by
invoking conspiracy theories or conflicting reports; it's well
established. But your story has it happening in 2102 instead. Hey,
multiverse!


How Does Core Expand?
------------------------------

The danger here is that Core will never grow beyond a certain
point, because if people start using it and adding to it, it will
become so complex that it buckles under its own lack of centralised
continuity.

To avoid stagnation or, conversely, irreparable splintering, I invite
anyone and everyone who wants to contribute to add to Core in the same
basic spirit that Core was written. I prefer that contributions are
given on an as-they-happen basis; contribute the product of something you
made, rather than just throwing in random events that happen at random
times, just for the sake of fleshing out the Core. Core should grow
from the stories being told; the stories should not have to tailor
themselves to Core.

World building is important, though, so if that's your thing, then
give it a go. It's probably best to develop subjects you actually know
about. For instance, if you don't programme then maybe don't make an
attempt to design the operating system of **Cyberlyptic**, but do
develop its fashion, if fashion design is something you do, and so
on.

