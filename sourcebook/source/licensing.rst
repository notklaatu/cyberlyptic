
Licensing 
==================

To provide maximum flexibility, it requires a layered model of
licensing.


Layered Licensing
------------------------

First of all: you never have to use the **Cyberlyptic** "brand" if
you don't want to. If you want to use the ideas here but not align
yourself with **Cyberlyptic** project, that's *fine*. You are
completely allowed to do that, no one will be angry or offended. The
point of this project is to foster creativity, not limit it, and not
to create drama (except the imaginary kind).

**Cyberlyptic** is also not about attribution or taking credit for
someone else's work. On the one hand, recognition is a powerful thing,
and if we all contribute to **Cyberlyptic** and also credit one
another, then we all become stronger as artists and crafters, and we
benefit from the team effort.

.. _cc0:

CC-0 Option: Core
^^^^^^^^^^^^^^^^^^^^^

It should never be suspected that **Cyberlyptic** is an attempt to
create a generic "cyberpunk" world and then take credit for everything
that ever looked in our direction. For this reason, **Cyberlyptic
Core** can always be licensed (outside of `trademarks <trademark>`_
and other `exceptions <except>`_ such as character names and any
events created by authors outside of **Core**), at your option, as
`CC-0 <https://creativecommons.org/about/cc0/>`_. No credit is
required; use what you want for any purpose, and never acknowledge
that **Cyberlyptic** exists. It's fine; no hard feelings, honest!

.. _ccby:

CC-BY Option: Core
^^^^^^^^^^^^^^^^^^^^^^^^^

There's no illusion that the ideas in **Cyberlyptic** are unique or
groundbreaking. This isn't a universe built from the ground up; it's a
creative commons universe that provides a rich backdrop for whatever
art any individual wants to create.

For this reason, **Cyberlyptic Core** is licensed `CC-BY
<https://creativecommons.org/licenses/by/4.0/>`_, meaning that you are
free to use the ideas of **Cyberlyptic Core** for any purpose. You can
make money off your work, you claim total ownership over the product
of your labour, you own the copyright, and you never have to give
back. If you choose to use the CC-BY license, then you should, even if
it's in tiny print, or even just somewhere in information *about* your
work, identify that you art is set in a **Cyberlyptic** universe.

If you do not want to credit us, you are welcome to use the :ref:`cc-0
<cc0>`_ license option instead, but if you do that, you lose access to
trademarks, character names, and other items that are individually
licensed (such as the :ref:`Federek <money>`_ system from `Epoch:Human
   <http://epoch-human.wikia.com/wiki/The_Federek>`_).

That's **Core**. It's the one part of **Cyberlyptic** that is common
ground; it's the one thing we all share from the **Cyberlyptic**
project.


Verses
^^^^^^^^^^^^^^

Everything else is either part of the **Cyberlyptic Universe** or
**Cyberlyptic Multiverse**. Since those works will have elements
entirely unique to individual creators, the licenses there will
vary. Some people may use `CC-BY-SA
<https://creativecommons.org/licenses/by-sa/4.0/>`_, others may use
`CC-0 <https://creativecommons.org/about/cc0/>`_, others still may not
allow you to use anything they create.

Let me break that down for you. If you're an artist and:

* you read a story labelled **Cyberlyptic Core**, then you can use
  anything and everything contained in that story for whatever
  purpose you want.
* you read a story labelled **Cyberlyptic Universe** or **Cyberlyptic
  Multiverse**, then you should check its license to make sure you
  comply with the author's wishes.

If anything from someone else's work is integrated into **Core**, it
will be requested that they license their work with a dual CC-0 and
CC-BY so that everyone can use the idea in future work. If that's not
acceptable to them, then it becomes an exception:


.. _except:
   
Trademark Exceptions
-------------------------

The most significant and common exception to all licenses are
character *names*. All characters created by an author are excempt from
the CC-BY license unless explicitly stated.

The reason for this exception is to safeguard out-of-character story
arcs from happening outside of the creator's control. For instance,
let's say I create a hard-boiled detective character named Rakk
Grollard who's lived a life of minor corruption and general sleeze
until he topples a major EnterCorp in a sudden moment of
redemption. Then someone else decides to use Rakk Grollard in a
spinoff story, writing a twist in which it turns out that he was
colluding with an opposing EnterCorp all along, thus completely
negating his redemption. That's obviously the opposite effect for that
character that I wanted, and it could completely ruin my intention for
future stories.

To avoid such misunderstandings, continuing characters or events
created by someone else are essentially off-limits unless permission
has been granted by the creator of that story element or by a license
that allows it.

Even when licenses or an artist grants permission to continue a story
arc or element, the official canon of that element lies with the
creator of that element.

Exceptions to the Exceptions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Major public figures or significant historical figures (they're marked
as **public figures**) that exist in Core are OK to use. You can use
their names, you can write them into your stories or games; they're
fair game because they're part of Core. If your story or project has
them doing things blatantly outside of established canon, then you're
obviously no longer writing within Core canon, but that's OK!


Stories About Stories
==============================

You've read this entire document, and now you're equal parts
enlightened and confused. What's the point of **Cyberlyptic**? Why
does it exist? Why do we want this?

**Cyberlyptic** exists as a baseline frame-of-reference so that when
an artist, video game programmer, RPG game designer, musician, illustrator, or anyone
else sits down and thinks "I should make this thing, and it should be
set in a futuristic dystopia!", they aren't burdened with the task of
re-inventing the wheel. **Cyberlyptic** lays out a world pre-built for
them, and they're free to ignore, change, or use whatever part of it
that they want.

It seems silly and almost like a cheat, but it's no more or less a
cheat than inventing some silly terms off the top of your head because
you needed something that sounded edgy and techy. With
**Cyberlyptic**, the silly terms are all written out for you, along
with backstory, atmosphere, and new potential.

Frankly, it's what all of us artists do *anyway*: we get inspired by
the work of others. The difference is, with **Cyberlyptic** there are
no copyright concerns, no concerns of stepping on anyone's toes, and a
sense of ownership. **Cyberlyptic** is as much yours as you want it to
be. Enjoy.

The important tenants to keep in mind about **Cyberlyptic** are:

1. Provide a commonly-created and commonly-owned tech-fantasy basis for everyone.
2. Embrace the chaos of having many versions of the same story; it's
   bound to happen, and it's OK because that's how real life works, too.

