People
=========

It takes all types, don't it? 


Business
---------

.. csv-table:: RPG Traits
   "HP",5
   "Attack", "1d6"
   

Call them "businesspeople" or call them "corpdrones", but the
EnterCorps run the world and all the little cogs inside of them are,
as such, beyond reproach. It's not that businessmen and women are
above the law, it's just that they *are* the law, and if it comes to
push and shove, the law sides with its own.

Generally speaking, businesspeople in the **cyberlypse** are the elite
class. Not many worked to achieve their status; it's mostly
inherited at the top levels. Those who attempt to move in on the
business world are usually made to settle for mid-level management at
best. Anyone else you see in an EnterCorp is either kidding
themselves, or indentured. 


Artists
--------

.. csv-table:: RPG Traits
   "HP",5
   "Attack", "1d6"

There are two kinds of artists in the **Cyberlypse**: those that make
money and those who don't. The ones who get any respect at all are, of
course, the ones that walk with money. The others aren't "real"
artists and therefore fall into other social classes and just make art
on the side, as a hobby. Whether or not this "hobby" comes in handy as
spendable blit is a different question. 

That's the social spectrum, but there are many disciplines. Sculptors
are needed to commemorate the wealthy, Pharaoh-style (in fact, the
Egyptian motif for EnterCorp-sponsored art is very hot at the moment),
and painters, for the same reason. There is a musical and immersvid "elite", too,
distributing music and full-immersion videos that a cynic might call
*the opiate of the masses*. As long as it makes favourable money back,
these artists earn the title of *celebrity*, and are accordingly
worshipped and respected.

In the lower ranks, the same skills exist, it's just all done for fun
or for blit. There's the addition of grafitti artists (although it's
by no means below the progressive EnterCorps to sponsor grafitti, as
long as it is *their* grafitti). 


Sports
-------

.. csv-table:: RPG Traits
   "HP",8
   "Attack", "2d6"

You mean gladiators?

With the dissolution of proper militaries, the excess testosterone had
to move over to one of two places: :ref:`Enterpol <police>` or
sports.

Sports in the **Cyberlypse** are played for keeps: you go out onto the
field and play to win, or you go out onto the field and die. No holds
barred, no :ref:`medics <medic>` on standby (except when they are; the wealthy
sportsmen employ private medics). Games are held as big events and
inspire rabid loyalty among fans, often providing a great excuse to
riot over losses or victories.

The usual sports like rugby, tackleball, hockey, wrastling, car and
bike racing are the mainstays, but there are plenty of new sports that
get developed just to keep people interested. Lots of combat and
daredevil arena events come and go, usually as a way to make
EnterCorp-designated executions make a little extra money on the
side.

To-the-death golf turned out to be less popular than anticipated, but
otherwise almost anything goes.


.. _police:

EnterPol
----------

.. csv-table:: RPG Traits
   "HP",8
   "Attack", "2d6"

There are no countrywide military forces or citywide police forces,
but there are innumerable **EnterPol** organisations. Each EnterPol
was created and sustained by an EnterCorp in need of on-site security,
and its reach may range from very local (literal on-site security), or
very widespread (citywide privatised police force). Sometimes an
EnterPol is welcomed in a community, because those that are just do
provide law and order, while other times they are corrupt, resented
even by the most obedient citizen.

There are no boundaries to an EnterPol's jurisdiction. Strictly
speaking (and certainly the originally intended purpose), an
EnterPol's jurisdiction are the property lines of the buildings they
protect. Since cities themselves became the properties of EnterCorps,
the EnterPol jurisdiction extended to the city street; allowing for
this, one EnterPol's jurisdiction stops at a city's limit.

In practise, EnterPols respect or ignore boundaries depending on what
serves them best. If an EnterPol has set its sights on apprehending a
"criminal", then no boundary will keep them at bay, although the guns
of the neighbouring EnterPol might. If, on the other hand, an EnterPol
is mostly just happy to have an undesirable citizen out of their city,
then the boundaries of jurisdiction is conveniently respected above
all else.

To sum it up, EnterPols are gangs at worst, militiae at best. There
have been rumours that some large EnterPols might work together, and
even conspiracy theories that some are governed by the same, secret
super-organisation. More likely, the EnterPols are too top-heavy and
too invested in their own parent company's interests to make deals
with any competing force, and if there *is* an inisible puppetmaster
controlling key EnterPol forces, it's the least efficient and most
conflicted puppetmaster possible.

Well, that's my opinion, anyway.


Gangs and Other Social Clubs
------------------------------

.. csv-table:: RPG Traits
   "HP",6
   "Attack", "2d6"

To paraphrase a once-great poet: *the world's a gang, and we are all
but thugs*. In the **Cyberlypse**, the only way you survive is to be a
member of a social club, labour union, collective, community centre,
or street gang. It doesn't much matter what fanciful name you give
them or what dues you charge for membership; they're all the same and
they all serve the same purpose: protect the interests of its members.

The problem with gangs and social clubs and community groups is that
generally anyone can claim allegiance (even if proper membership
has specific requirements), and not everyone who supports their local
gang knows exactly what their local gang actually supports
itself. The result is often that anything *new* or different is
regarded as undesirable, whether or not the local community gang would
benefit from it.

Most formalised gangs and clubs have mesh-sites with official
statements about their stance and their work. Some are friendly,
others are not. Some are free to join, some cost big rek. Some are
well organised, others are wrought with splinter groups working on
conflicting objectives under the same banner. It's like an EnterCorp,
but without the certification.

Pragmatically, it's impossible *not* to be in a gang or social club of
some sort; even if you consciously avoid membership to any group,
outsiders are going to lump you into a meta-gang of anti-gang
loners. Loners do *not* have a gang, but that doesn't mean other
people won't assume they know everything about you already based on
the fact that you're a loner. In other words, no matter what, you're
assigned to an EnterCorp or a gang, even when no gang exists, so you
may as well embrace it, even if only to reject it.


.. _medic:

Medics
-------

.. csv-table:: RPG Traits
   "HP",12
   "Attack", "1d6"

Medics specialise in bio and biogenetic repair. Simply put, if you get
cut by a street thug, a medic'll patch you up. If you lose an arm. a
medic can grow you a new one. For a price. Always for a price.

Basic biological repair is universal to all medics, but the biogenetic
side of things gets more complex. Those employed and trained by an
EnterCorp, their knowledge is generally limited to the biogenetic code
that their own EnterCorp uses. And since they have access to all the
source code from the EnterCorp mesh, they know the biocode very, very
well. But don't even ask about doctor-patient confidentiality; an
EnterCorp medic is bound by EnterCorp law to report all incidents, no
matter what (in fact, few have the ability to choose whether they
report it or not, since all patients are monitored upon entering a
medic's office).

Street medics, on the other hand, may have a broader talent for
biocode, but a less in-depth knowledge of it, since their knowledge is
reverse-engineered. There's an active mesh for medics to share
knowledge, although it's regularly attacked by EnterCorp hackers,
since the information is copyrighted and, as such, illegal to share.

Street medics are more likely to feel a moral obligation to treating
you, too. EnterCorp medics are more likely to see patients as
meatware rather than humans; if the cost of replacement parts exceeds
the total expected return in productivity, it'll be written off as a
business loss. That's medic-speak for "dead on arrival".


Hackers
--------

.. csv-table:: RPG Traits
   "HP",6
   "Attack", "2d6"

The mesh is an exciting place: so many targets aimlessly wandering
around, so few with any real understanding of how it all works.

The hackers dwelling with the **Cyberlypse** are arguably the
lifeblood of the mesh. They're the ones who, it the wake of the Y2K
collapse, were able to keep the mainframes running, and then pull and
pool them together to form local meshes, and then meshes of meshes,
enabling the world to communicate with the same ease as it had before
the crash.

Once the EnterCorps moved in and effectively took over, the hackers
either got jobs with the EnterCorps, or they retreated
underground. And that's exactly where they remain today.

Despite popular mythos, the best hackers rarely work alone. The mesh
is complex, and no one hacker ever knows everything there is to know
about every one of the myriad technologies the mesh is made of. For
this reason, hacker gangs usually include a meshwork specialist that
navigates the web of meshes and mesh-sites, a programmer to
deconstruct firewalls and paywalls, a cryptographer to intelligently
brute force passwords and puzzles, and possibly an electrician to
manipulate raw electrons.

There are three types of hackers you're likely to encounter:

* White hats: EnterCorp hackers, working for rek, doing whatever their
  bosses tell them to do. If you ask them, they are uphold traditional
  values and morals, using computers for Good. If you ask me, I'd
  diplomatically decline to comment.
* Red hats: Independent hackers, working for some cause other than
  rek. Their causes aren't always altruistic, but they aren't for the
  EnterCorps, either. If you ask them, they're the true definition of
  a "hacker", using computers as computers for computing sake.
* Blue hats: A less common term, a blue hat hacker is essentially the
  same as a Red hat hacker, but with the connotation that they are
  working on a completely independent level, affiliating with other
  hackers rather than with any cohesive gang. It's a useful term to
  differentiate between independent mesh consultants (red hats) and a
  random kid on the street who happens to be seriously good on the
  mesh (blue hat). 
* Gray hats: Mercenaries. At any given time, a gray hat might be
  working for an EnterCorp, a hacker gang, or no one at all.
* Black hats: A hacker promoting utter chaos. Usually considered
  unhealthy for the mesh-at-large, a black hat hacker deals damage to
  both the EnterCorps and the mesh itself, basically ruining
  everything for everyone. They're the kids in school who used to
  misbehave and get recess privileges revoked for the whole class;
  even if you respect what they're doing, you still want to beat them
  up after school.
.. _commies:
* Commies: Communications Technicians ("commies") specialise in the
  underlying technologies of the mesh. They don't tend to know their
  way around the mesh itself, because they operate under the mesh, so
  if you need someone to punch a hole in the meshwork or hijack an
  entire local mesh, a commie is who you'd turn to. Commies are an
  old-world, antisocial group, mostly existing in the Outlands. Most
  people ignore them, even though we all end up using the technology
  they develop.


Mesh Heads
^^^^^^^^^^^^^^^^^^^

"Mesh heads" is slang for a *mesh addict*, if you're a "normal" person, or
for *any given mesh user* if you're a hacker. They live their lives in the mesh without
any understanding of the technology that makes it possible. They tend
to be loud, obnoxious, and on the wealthy end of the spectrum (since
no one without rek could afford to spend their entire day in the
mesh).




Working Class
------------------

.. csv-table:: RPG Traits
   "HP",5
   "Attack", "1d6"

You name it, the EnterCorp and the streets have it. If you've just
wandered into town and are looking for work, then your first stop is
local businesses. They don't pay much, but they usually offer blit or
room and board. You can usually find work as a shop assistant,
janitor, mechanic, facility maintenance, or whatever it is that you
do. This is all street-level, though, so it's not exactly a career,
it's just stuff people gravitate toward because humans have some basic
needs, and the EnterCorps don't necessarily provide everything to all
people.

If it's a fast track to the lower-middle class you want, then you'll
want to look at getting work in an EnterCorp franchise business. Do
well there, and you might eventually get transferred into your
EnterCorp campus, where all the lights are bright and everyone is
happy and wealthy. Everyone but you, that is; you'll be at the bottom
of the ladder, serving your EnterCorp masters with a smile and a
blush. Keep doing that until you retire, and your children will take
your place (as long as you can keep out the riffraff from the streets
trying to do the same thing you did), and eventually your children
might move up within the EnterCorp ranks. It might take a few
generations, but darn it, eventually your legacy will be wealthy,
vapid, and beautiful meatware living the high life in the EnterCorp
mothership. Good luck!


Outlanders
------------

.. csv-table:: RPG Traits
   "HP",7
   "Attack", "2d6"

Not everyone got sucked up by Y2K. The places that were not useful to
the EnterCorps became known as the Outland. At first, it was a
derogatory term invented by the EnterCorps to encourage people to move
into the cities, under the protection and care of the EnterCorps. When
the cities turned out to be slums lying at the feet of the wealthy
EnterCorp citadels, a lot of people started wondering if maybe the
Outlands had something going for them, after all.

In reality, the term "outland" is a broad one. It refers to any
territory not governed by an EnterCorp. Notice the use of the word
*governed* rather than "owned". On paper, almost every EnterCorp claims
ownership of Outland territory, but whether or not the EnterCorp sends
EnterPol out into the outland, or even has any franchises or interests
in the outland at all, usually betrays just how solid that "ownership"
really is. Outlanders learned long ago that possession is 99% of
ownership, so the outlands belong to the outlands.

Some parts of the outlands rival the technology and prosperity of big
EnterCorp cities, and others are wastelands where only the strong have
a chance of survival. That's the spectrum; globally, every imaginable
variation exists.

Outlanders tend to be seen as hillbillies or rednecks, at least by
city folk. In reality, they may or may not lack street smarts; and
depending on what their goal in the city is, that may or may not
matter. What an Outlander definitely is *not* is EnterCorp
property. I've heard of one or two Outlanders score a consultation job
with an EnterCorp, but those have always been under the table deals,
and the circumstances extenuating. Generally speaking, EnterCorp and
Outland don't mix.
