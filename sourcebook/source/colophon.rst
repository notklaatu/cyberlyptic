Colophon
----------

This document was written entirely on an open source stack: everything
from the operating system to the text editor.

* The operating system in use was Linux, mostly
  [Slackware](http://slackware.com) with
  [Slackermedia](http://slackermedia.ml) mods, but sometimes on
  [Ubuntu](http://ubuntu.com) or similar, depending purely on what
  computer was available.
* The text editor used was GNU [Emacs](http://gnu.org/software/emacs),
  a free and insanely robust and modular text editor with a plugin for
  everything you could ever want to do on a computer, much less a text
  editor.

It was written in ReStructured Text (rST), a form of "markdown" that
seeks to not only provide a sensible page layout but also to be
readable even as plain text. It's not quite as unobtrusive as, say
[asciidoc](http://powerman.name/doc/asciidoc) but it's close enough,
and is the format that is used by the text processor used for the
conversion of all this plain ol' text into a proper book form:
[Sphinx](http://www.sphinx-doc.org/en/stable).

Book cover layout and design was done with [GIMP](http://gimp.org),
[Inkscape](http://inkscape.org), and [Scribus](https://www.scribus.net).
