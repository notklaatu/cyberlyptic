Money and Ethics
======================

The first thing you need to know is that the world of the
**Cyberlypse** is governed by money. No surprise there; the world's
always been wealth-centric, but in the **Cyberlypse**, it's
official. God isn't dead, God is Wealth.

So we might as well start there.

Money is the only thing in the world that truly cares about justice,
personal security, health, happiness. Only through money are all of these
things possible. If you don't have money, you can't provide for your
family or your community. Without money, you cannot achieve anything
*good*.

If you are not interested in Money, then you are an enemy to our goal
of building a better society. You're an enemy of helping the sick and the
needy. You're not welcome in the **Cyberlypse**.

That's what they'll tell you, anyway. Not in so many words, but the
good and decent people around here take this very seriously, so get
used to it. Even the people who don't take it seriously, the ones who
claim not te be religious, still take it seriously, deep down. If you
don't care about Money, you're basically a nihilist and nobody likes a
nihilist.

You see, folk 'round here believe that for thousands of years, humans
had it wrong: God doesn't hate money; God *is* Money. But it's not
something you go and sign up for, it's how you're raised. It's in your
schooling, it's in your job, it's the basis for all the holidays. This
is just how it is. Money is God, coins are His blessings, and we are
all his humble servants. The more money we make in this life, the
better it'll be for us in the afterlife; you reap what you sow.

For pedants: the reason the ancient gods of so many religions were
busy telling people to forsake their belongings and money was
because God wanted you to forsake all money but Him. In other words,
don't go valuing silly things like your time, your life, your sanity,
your family. To do that is to build a wall between you and the holy,
true, and complete comprehension of the glory of Money.

Money's holy will is administered by His churches, the EnterCorps. Of
course there's only one true EnterCorp, and of course which one that
is depends on who you ask.

The CEO of any EnterCorp is essentially a high priest. They aren't
usually called a "priest" or "pope" (although, admittedly, each
EnterCorp differs), but it's common practise for their employees to
refer to them as "Father" or "Mother" as a sign of respect.

God's blessings are bestowed upon people in the form of, as you might
guess, payment. Whether you're pulling a paycheck from your EnterCorp
or whether you're paying for a good or service, or even just tipping a
waitress, you're either receiving or giving a blessing.

.. note::
   The usual religious questions, of course, pop up every now and again,
   but to the rational mind they are easily answered.

   For instance, if God loves the world so much, why are there still poor
   people? That's a simple one. Poor people don't exist because they
   *can't* get money, they exist because they *won't* accept the
   bountiful wealth trying to bestow itself upon them. If they would only
   open up to the power of the Lord Federek, they would become
   wealthy.

   So it's not just harmful to society and to a poor person to give
   alms, it's unethical and wrong. Any college student activist will
   tell you that. *Save the world: hoarde your cash.*


Federek
-----------

The Federek is an entirely digital currency. It is never a physical
thing, and in most cities and small towns, it's considered insulting
and possibly even blasphemous or subversive to offer an exchange or
barter for something that could be bought with Federek.

.. note::

   The Federek system was created for `Epoch:Human
   <http://epoch-human.wikia.com/wiki/The_Federek>`_, through a
   collaboration between `Thirdilemma
   <http://www.reddit.com/user/Thirdilemma>`_, `WhatWhatHunchHunch
   <http://www.reddit.com/user/WhatWhatHunchHunch>`_, and
   `ViolatorMachine
   <http://www.reddit.com/user/ViolatorMachine>`_. Its religious
   nature and the culture around that, however, is **Cyberlyptic** only.

The Federek System encompasses the officially agreed upon world
currency. It is managed by an organisation also called the **Federal**, or
**EnterFed** in street lingvo. **EnterFed** manages, tracks, and
insures all **Federek** transactions.

The **Federal** is a global organisation and holds its power only by
default; they're the easy, go-to money exchange, so they hold any and
all power stemming from that. They hold no actual power, at least on
paper; whether or not they have an unfair advantage and influence over
other EnterCorps, obviously, is the subject of much debate.

Officially, the Federek uses a stylized ``F`` as its currency symbol:

.. image:: federek.png

Lacking that symbol on keyboards, the *ḟ* (that's Unicode
``0x1E1F``, or Compose-dot-f on Linux) character is acceptable, or
just *f* in a pinch.

The official name of the currency is "Federek". In polite company, you
refer to it as "Federek". Anything less is extremely offensive.

Out on the streets and in the Outlands, it's a different story. The
vulgar terms for it, there, are "rek", "rekko", "rekky", or even a
"fed", depending on local slang habits. These are also considered swear
words, as well, since the Federek is regarded as a diety, and giving
it a nickname is taking its name in vain. Not safe for work.

The exact monetary value of the Federek fluctuates on an almost hour-by-hour
basis. Manipulation of the worldwide stock markets by computerised
trading systems create fund fluctuations too fast for our feeble human
minds to follow.

People being people, there are two opposing systems to the almighty
Federek.


SafeHaven
^^^^^^^^^^^^

**SafeHaven** is an underground network for monetary exchange. It's
wildly controversial for many reasons.

This as a hardware-based monetary system that logs goods
and services for exchange on a series of mobile mainframes, enabling
transfer from peer to peer outside the Federek system. Its official
purpose is to provide an assurance of monetary backup should the
Federek ever fall (not entirely unlikely, given the competitive nature
of the EnterCorps involved) but its practical use is mostly for those
times that you need to transfer money without the transfer being
tracked. If you know what I mean.

SafeHaven gets flak from the other end of the spectrum, too,
though. The serious subversive types dislike SafeHaven because even in
its rebellion, it supports the Federk system. This "all or nothing"
argument gave rise to a second form of money in the **Cyberlypse**:
the :ref:`blit`_.

.. _blit:

Blit
^^^^^^^

*Blit* is deep-underground "money". It's a digital system of,
essentially, trackable and trade-able karma.

Each blit is a portion of an I.O.U. ("I owe you"). When someone gains
enough of your blit, they are allowed to call upon you *at any time*
to cash in a favour, according to the skills you list on your
electronic blitsite.

Cashing in a favour is called a *blitzkrieg*.

Blits themselves can be traded and used as payment. If I have a bunch
of blit from someone who can do building maintenance but I live in my
car, then it might be better for me to pay those blitz to someone who
finds them valuable. This means that if you use blit, it's not
uncommon to get vidcalls or email from total strangers who got your
blit from someone else, and now wants to use the blit to get something
done.

Blit is insured by the **Blitzkrieg** mesh. It's non-centralised, and
there is no EnterBlitzkrieg or anything like that. Transaction hashes
are kept out in the open, for everyone to see and synchronise to. This
ensures that someone isn't going around town paying with blit they
never intend to make good on, but also that some *skank* (a black hat
hacker) isn't forging transactions.

User feedback is tantamount to **Blitzkrieg**; you get a bad rep and
your blit is worthless. Get a good rep, and people who need your
talents don't just want your talent, they compete for it, effectively
making your blit more market-valuable.

The actual "value" of your blit is up to you. If you're a meshworking
savant and don't mind fixing people's local mesh, but you can't be
bothered to make yourself a damn cup of choffee (that's coffee, made
from chicory instead of coffee), then you might give an entire blit
worth 3 hours of work on a meshwork to get an afternoon of "free"
choffee. If a café owner has no need of your mesh services, the
chances are your blit will still be good for at least a cup or two,
with a high likelihood of it later being used to pay off a debt that
the café owes to someone who does need your mesh services.

All of these transactions are verified with unique hashes and explicit
authorisation from the current blit owner, and it's all double-checked
against the Blitzkrieg mesh. It's probably possible to hack some blit for
yourself (in theory) but the mesh doesn't appreciate it, and the risk of gaining
a bad rep is usually too great for anyone to bother trying.

Officially, **Blitzkrieg** is above-board and on-the-level. It's a
harmless bartering system that originated within the tech sector. It
only got ugly when people on the street and in the Outlands adopted it
as their currency, and actually started using blit instead of
rek. That's when the law stepped in, but the law only extends so deep
in the **Cyberlypse**, and blit is an alternative currency in heavy
use in some areas of every city, and is even the primary currency in
much of the Outland.

As an alternative system to the Federek, be careful who you offer blit
to. At best, you'll insult a nice, law-abiding citizen. There's not
really a ceiling on the *worst* possibility.
